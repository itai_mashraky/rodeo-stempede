using UnityEngine;
using System;

public class TargetManager : MonoBehaviour
{
    [SerializeField] GameObject[] Animals;

    [SerializeField] private GameObject[][] Targets;

    private void Awake()
    {
        int rowLen;

        Animals = GameObject.FindGameObjectsWithTag("Animal");
        Array.Sort(Animals, CompareByName);

        GameObject[] temp;

        rowLen = Animals.Length;
        print(rowLen);
        Targets = new GameObject[Animals.Length][]; // col amount for target

        for (int i = 0; i < rowLen; i++)
        {
            // targetArray
            temp = GameObject.FindGameObjectsWithTag("Target " + i);


            Array.Sort(temp, CompareByName);

            Targets[i] = temp;

        }
    }

    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }

    public GameObject[] getTargetArray()
    {
        GameObject[] ArrayToReturn = new GameObject[Targets[0].Length];

        for (int i = 0; i < Targets[0].Length; i++)
        {
            ArrayToReturn[i] = Targets[UnityEngine.Random.Range(0, Animals.Length)][i]; 
        }
        return ArrayToReturn;
    }

    public int CompareByName(GameObject a, GameObject b)
    {
        return -    a.transform.name.CompareTo(b.transform.name);
    }

}
