using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LassoTriggerZone : MonoBehaviour
{
    [SerializeField] private bool CanJump;
    [SerializeField] private AnimalController ActiveAnimal;
    [SerializeField] private PlayerFollow playerFollowScript;

    // Start is called before the first frame update
    void Start()
    {
        CanJump = false;
        playerFollowScript = GameObject.Find("Player").GetComponent<PlayerFollow>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && CanJump)
        {
            playerFollowScript.GetActiveAnimal().GetComponent<AnimalController>().SwitchControll();
            playerFollowScript.SetActiveAnimle(ActiveAnimal.transform);
            ActiveAnimal.SwitchControll();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        print("asdsad");
    }

    private void OnTriggerEnter(Collider other)
    {
        print("goof");
        if (other.CompareTag("Animal"))
        {
            ActiveAnimal = other.GetComponent<AnimalController>();
            CanJump = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Animal"))
        {
            ActiveAnimal = null;
            CanJump = false;
        }

    }
}
